# TF-Exporter
TF-Exporter is a forked addon that we have customized for Breachers in helping you export your level.
TF-Exporter works together with TF-Tools. You'll need both of these to work correctly when creating levels.

TF-Tools can be found [here](https://gitlab.triangle-factory.be/triangle-factory/blender-tools/blender-triangle-tools).

More information about TF-Exporter and how it works can be found on our [Confluence Page](https://triangle-factory.atlassian.net/wiki/spaces/B/pages/1092550658/TF-Exporter).
