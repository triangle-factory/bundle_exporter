import bpy
import imp

from . import modifier

from mathutils import Euler
from .. import settings

import math

class TF_CustomPropertyCollection(bpy.types.PropertyGroup):
    key: bpy.props.StringProperty(name="Key", default="")
    value: bpy.props.StringProperty(name="Value", default="")

class TF_UL_CustomPropertyCollection(bpy.types.UIList):
    def draw_item(self, context, layout, data, item, icon, active_data, active_propname, index):
        row = layout.row(align=True)
        row.prop(item, 'key', text='')
        row.prop(item, 'value', text='')

    def invoke(self, context, event):
        pass

class TF_OT_add_custom_property(bpy.types.Operator):
    bl_idname = "ezb.add_custom_property"
    bl_label = "Add Custom Property"
    modifier_bundle_index: bpy.props.IntProperty()

    def execute(self, context):
        from ..core import get_modifier_for_ctx
        ctx_modifiers = get_modifier_for_ctx(self.modifier_bundle_index)
        mod = ctx_modifiers.TF_modifier_custom_properties
        mod.custom_properties.add()
        index=len(mod.custom_properties) - 1
        mod.custom_properties_index = index
        return {'FINISHED'}

class TF_OT_remove_custom_property(bpy.types.Operator):
    bl_idname = "ezb.remove_custom_property"
    bl_label = "Remove Custom Property"
    modifier_bundle_index: bpy.props.IntProperty()

    def execute(self, context):
        from ..core import get_modifier_for_ctx
        ctx_modifiers = get_modifier_for_ctx(self.modifier_bundle_index)
        mod = ctx_modifiers.TF_modifier_custom_properties
        mod.custom_properties.remove(mod.custom_properties_index)

        if mod.custom_properties_index >= len(mod.custom_properties):
            mod.custom_properties_index = len(mod.custom_properties)-1
        return {'FINISHED'}

class TF_mod_custom_properties(modifier.TF_mod_default):
    label = "Recursive set custom properties"
    id = 'custom_properties'
    url = "http://renderhjs.net/fbxbundle/"
    type = 'GENERAL'
    icon = 'NODE'
    tooltip = 'Sets custom properties on all meshes if they don\'t yet exist'
    priority = 9999999
    dependants = [TF_CustomPropertyCollection, TF_UL_CustomPropertyCollection, TF_OT_add_custom_property, TF_OT_remove_custom_property]

    active: bpy.props.BoolProperty(
        name="Active",
        default=False
    )

    show_info: bpy.props.BoolProperty(
        name="Show Info",
        default=True
    )

    custom_properties: bpy.props.CollectionProperty(type=TF_CustomPropertyCollection)
    custom_properties_index: bpy.props.IntProperty(name="Custom Properties", default=False, description="Custom Properties")

    def _draw_info(self, layout, modifier_bundle_index):
        row = layout.row(align=True)
        row.template_list("TF_UL_CustomPropertyCollection", "", self, "custom_properties", self, "custom_properties_index", rows=2)
        col = row.column(align=True)
        addButton = col.operator('ezb.add_custom_property', text='', icon = 'ADD')
        addButton.modifier_bundle_index = modifier_bundle_index
        removeButton = col.operator('ezb.remove_custom_property', text='', icon = 'REMOVE')
        removeButton.modifier_bundle_index = modifier_bundle_index        

    def process(self, bundle_info):
        all_objects = bundle_info['meshes'] + bundle_info['empties'] + bundle_info['armatures'] + bundle_info['extras']
        for kvp in self.custom_properties:
            for obj in all_objects:
                #if not kvp.key in obj:
                obj[kvp.key] = kvp.value
            
