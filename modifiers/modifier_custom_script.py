import bpy
from bpy import path
from bpy.types import bpy_struct
from . import modifier


class TF_OT_show_custom_script_info(bpy.types.Operator):
    bl_idname = "tf.show_custom_script_info"
    bl_label = ""

    @classmethod
    def description(cls, context, properties):
        return 'Two global variables are passed to the script when run:\nbpy : blender main module\nbundle_info : dictionary containing all the data for the current bundle'

    def draw(self, context):
        layout = self.layout
        col = layout.column(align=True)
        col.label(text='Two global variables are passed to the script when run:')
        col.label(text='bpy : blender main module')
        col.label(text='bundle_info : dictionary containing all the data for the current bundle')
        col.label(text='     name : the name of the bundle')
        col.label(text='     pivot : the pivot of the bundle')
        col.label(text='     empties : the empties of the bundle')
        col.label(text='     armatures : the armatures of the bundle')
        col.label(text='     extras : used for storing special objects, like colliders')
        col.label(text='     export_format : the format used for exporting')
        col.label(text='     export_preset : dictionary containing the export preset information')

    def invoke(self, context, event):
        return context.window_manager.invoke_props_dialog(self, width=900)

    def execute(self, context):
        return {'FINISHED'}


class TF_mod_runscript(modifier.TF_mod_default):
    label = "Custom Script"
    id = 'custom_script'
    type = "GENERAL"
    icon = 'TEXT'
    tooltip = 'Run a script before exporting'

    active: bpy.props.BoolProperty(
        name="Active",
        default=False
    )

    show_info: bpy.props.BoolProperty(
        name="Show Info",
        default=True
    )

    custom_priority: bpy.props.IntProperty(
        name="Priority",
        default=999,
        description='Modifiers with higher priority number will be executed later, lowest is -9999, highest is 999'
    )

    script: bpy.props.PointerProperty(
        name="Script",
        type=bpy.types.Text
    )

    dependants = [TF_OT_show_custom_script_info]

    @property
    def priority(self):
        return self.custom_priority

    def _warning(self):
        return not self.script

    def _draw_info(self, layout, modifier_bundle_index):

        layout.operator('tf.show_custom_script_info', text='', icon='INFO')

        col = layout.column(align=True)
        col.use_property_split = True
        col.prop(self, 'custom_priority')
        col.prop(self, "script")

    def process(self, bundle_info):
        if self.script:
            code_globals = {'bpy': bpy, 'bundle_info': bundle_info}
            code_locals = dict()
            code = compile(self.script.as_string(), self.script.name, 'exec')
            exec(code, code_globals, code_locals)
