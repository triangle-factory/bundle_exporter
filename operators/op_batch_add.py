import bpy

class TF_OT_add_batch(bpy.types.Operator):
    """Create new bundle"""
    bl_idname = "tf.add_batch"
    bl_label = "Add Batch"

    def execute(self, context):
        bpy.context.scene.TF_Settings.batches.add()
        return {'FINISHED'}