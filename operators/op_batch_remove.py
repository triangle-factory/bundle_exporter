import bpy

class TF_OT_delete_batch(bpy.types.Operator):
    """"""
    bl_idname = "tf.delete_batch"
    bl_label = "Delete Batch"

    index: bpy.props.IntProperty()

    def execute(self, context):
        bpy.context.scene.TF_Settings.batches.remove(self.index)
        return {'FINISHED'}