import bpy
import os
import mathutils
import math
import aud

import pathlib

from .. import modifiers
from .. import bundles

prefix_copy = "EXPORT_ORG_"

#Audio
addon_dir = os.path.dirname(os.path.realpath(__file__))
errorSoundPath = os.path.join(addon_dir, "..\\sounds\\error.wav")
sucessSoundPath = os.path.join(addon_dir, "..\\sounds\\success.wav")

device = aud.Device()
errorSound = aud.Sound(errorSoundPath)
sucessSound = aud.Sound(sucessSoundPath)

def PlaySound(type):
    if bpy.context.preferences.addons["tf-exporter"].preferences.playAudio == False:
        if type == "ERROR":
            handle = device.play(errorSound)
        if type == "SUCCESS":
            handle = device.play(sucessSound)

def ShowMessageBox(message = "", title = "Message Box", icon = 'INFO'):
    def draw(self, context):
        self.layout.label(text=message)

    bpy.context.window_manager.popup_menu(draw, title = title, icon = icon)

class TF_OT_file_export(bpy.types.Operator):
    bl_idname = "tf.file_export"
    bl_label = "export"
    bl_description = "Export All Bundles"

    @classmethod
    def poll(cls, context):
        if context.space_data.local_view:
            return False

        if bpy.context.scene.TF_Settings.path == "":
            return False

        if len(bundles.get_bundles(only_valid=True)) == 0:
            return False
        
        if context.scene.TF_Settings.oldExportBool == False:
            if context.preferences.addons["tf-exporter"].preferences.fork_directory == "":
                return False

        return True

    @classmethod
    def description(cls, context, properties):
        if context.space_data.local_view:
            return "Can't export in local view"

        if bpy.context.scene.TF_Settings.path == "":
            return "Can't export if the export path is not set"

        if len(bundles.get_bundles(only_valid=True)) == 0:
            return "No bundles to export"

        return "Export All Bundles"

    def execute(self, context):
        if context.scene.TF_Settings.overrideExportCheck == False or context.scene.bool_DisableForScene == True:
            try:
                bpy.ops.qabot.quick()
            except:
                PlaySound("ERROR")
                ShowMessageBox("tf-tools is probably not installed or QA Bot has an error.", "Export cancelled", "ERROR")
                return {'FINISHED'}

            if context.scene.int_QABotErrorCount > 0:
                PlaySound("ERROR")
                ShowMessageBox("Please resolve all QA bot errors before exporting.", "Export cancelled", "ERROR")
                return {'FINISHED'}
            
        # Warnings
        if bpy.context.scene.TF_Settings.path == "":
            self.report({'ERROR_INVALID_INPUT'}, "Export path not set")
            return {'CANCELLED'}

        bundle_list = bundles.get_bundles(only_valid=True)
        returnVar = bundles.exporter.export(bundle_list)

        if returnVar == "{'Most likely something is wrong with the export path.'}":
            PlaySound("ERROR")
        else:
            PlaySound("SUCCESS")

        return {'FINISHED'}


class TF_OT_file_export_scene_selected(bpy.types.Operator):
    bl_idname = "tf.file_export_scene_selected"
    bl_label = "export selected"
    bl_description = "Export Selected Bundles"

    @classmethod
    def poll(cls, context):
        if context.space_data.local_view:
            return False

        if bpy.context.scene.TF_Settings.path == "":
            return False

        if len(bpy.context.scene.TF_Settings.bundles) == 0:
            return False

        if len([x for x in bundles.get_bundles() if x.is_bundle_obj_selected()]) == 0:
            return False
        
        if context.scene.TF_Settings.oldExportBool == False:
            if context.preferences.addons["tf-exporter"].preferences.fork_directory == "":
                return False

        return True

    @classmethod
    def description(cls, context, properties):
        if context.space_data.local_view:
            return "Can't export in local view"

        if bpy.context.scene.TF_Settings.path == "":
            return "Can't export if the export path is not set"

        if len(bpy.context.scene.TF_Settings.bundles) == 0:
            return "No bundles to export"

        if len([x for x in bundles.get_bundles() if x.is_bundle_obj_selected()]) == 0:
            return "No bundle selected"

        return "Export selected bundles"

    def execute(self, context):
        if context.scene.TF_Settings.overrideExportCheck == False or context.scene.bool_DisableForScene == True:
            try:
                bpy.ops.qabot.quick()
            except Exception as error:
                PlaySound("ERROR")
                ShowMessageBox("tf-tools is probably not installed or QA Bot has an error.", "Export cancelled", "ERROR")
                print(error)
                return {'FINISHED'}

            if context.scene.int_QABotErrorCount > 0:
                PlaySound("ERROR")
                ShowMessageBox("Please resolve all QA bot errors before exporting.", "Export cancelled", "ERROR")
                return {'FINISHED'}
            
            
        export_bundles = [x for x in bundles.get_bundles() if x.is_bundle_obj_selected()]
        returnVar = bundles.exporter.export(export_bundles)
        if returnVar == "{'Most likely something is wrong with the export path.'}":
            PlaySound("ERROR")
        else:
            PlaySound("SUCCESS")
        
        context.scene.TF_Settings.overrideExportCheck = False

        return {'FINISHED'}



class TF_OT_file_export_selected(bpy.types.Operator):
    bl_idname = "tf.file_export_selected"
    bl_label = "export selected"
    bl_description = "Export Selected Bundles"

    @classmethod
    def poll(cls, context):
        if context.space_data.local_view:
            return False

        if bpy.context.scene.TF_Settings.path == "":
            return False

        if len(bpy.context.scene.TF_Settings.bundles) == 0:
            return False

        if not(bpy.context.scene.TF_Settings.bundle_index < len(bundles.get_bundles()) and len(bundles.get_bundles()) > 0):
            return False

        return True

    @classmethod
    def description(cls, context, properties):
        if context.space_data.local_view:
            return "Can't export in local view"

        if bpy.context.scene.TF_Settings.path == "":
            return "Can't export if the export path is not set"

        if len(bpy.context.scene.TF_Settings.bundles) == 0:
            return "No bundles to export"

        if not(bpy.context.scene.TF_Settings.bundle_index < len(bundles.get_bundles()) and len(bundles.get_bundles()) > 0):
            return "No bundle selected"

        return "Export {}".format(bundles.get_bundles()[bpy.context.scene.TF_Settings.bundle_index].filename)

    def execute(self, context):
        bundles.exporter.export([bundles.get_bundles()[bpy.context.scene.TF_Settings.bundle_index]])

        return {'FINISHED'}
